import { CLICK_FILTER, INPUT_STRING_FILTER } from "../constants/task.constants"


export const inputStringFilter = (inputValue) => {
    return {
        type: INPUT_STRING_FILTER,
        payload: inputValue
    }
}

export const clickButtonFilter = () => {
    return {
        type: CLICK_FILTER
    }
}
