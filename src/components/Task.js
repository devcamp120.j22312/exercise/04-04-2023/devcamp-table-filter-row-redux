import { Grid, Button, TextField, Table, TableHead, TableRow, TableCell, TableContainer, TableBody, Paper, Container } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import { inputStringFilter, clickButtonFilter } from '../actions/task.action'
import data from '../assets/data/Data'


const Task = () => {
    const dispatch = useDispatch();

    const {inputFilter, taskList, status, id} = useSelector((reduxData) => {
        return reduxData.taskReducer;
    })

    const onInputFilterChangeHandler = (event) => {
        dispatch(inputStringFilter(event.target.value));
    }

    const onButtonFilterClickHandler = () => {
        dispatch(clickButtonFilter());
    }


    return (
        <Container>
            <Grid container mt={5} alignItems="center">
                <Grid item xs={3} md={3} lg={3} sm={3} textAlign="center">
                    <p>Nhập nội dung lọc</p>
                </Grid>
                <Grid item xs={6} md={6} lg={6} sm={6}>
                    <TextField label="Input filter here" fullWidth onChange={onInputFilterChangeHandler} value={inputFilter}></TextField>
                </Grid>
                <Grid item xs={3} md={3} lg={3} sm={3} textAlign="center">
                    <Button variant="contained" onClick={onButtonFilterClickHandler}>Lọc</Button>
                </Grid>
            </Grid>

            <TableContainer style={{ marginTop: "20px" }} component={Paper}>
                <Table>
                    <TableHead>
                        <TableRow container>
                            <TableCell>STT</TableCell>
                            <TableCell>Nội dung</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {status === true ? 
                        <TableRow container>
                            <TableCell>{1}</TableCell>
                            <TableCell>{taskList}</TableCell>
                        </TableRow>
                        : data.map((value, index) => {
                            return (
                                <TableRow container>
                                    <TableCell>{index + 1}</TableCell>
                                    <TableCell>{value.noiDung}</TableCell>
                                </TableRow>
                            )
                        })
                    }
                        {/* {data.map((element, index) => {
                            return (
                                <TableRow container>
                                    <TableCell>{data[index].STT}</TableCell>
                                    <TableCell>{data[index].noiDung}</TableCell>
                                </TableRow>
                            )

                        })} */}
                    </TableBody>
                </Table>
            </TableContainer>
        </Container>
    )
}

export default Task;
