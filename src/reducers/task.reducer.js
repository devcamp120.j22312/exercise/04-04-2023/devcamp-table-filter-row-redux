import { CLICK_FILTER, INPUT_STRING_FILTER } from "../constants/task.constants";
import data from '../assets/data/Data'

const initialState = {
    inputFilter: "",
    taskList: "",
    status: false,
    id: 0
}

const taskReducer = (state = initialState, action) => {
    switch (action.type) {
        case INPUT_STRING_FILTER:
            state.inputFilter = action.payload;
            break;
        case CLICK_FILTER:
            if (state.inputFilter == "") {
                state.status = false;
                break;
            }

            var result = data.filter(({ noiDung }) => noiDung.toLowerCase() === state.inputFilter.toLowerCase());
            if (result.length === 0) {
                state.status = false;
                break;
            }
            result.map((value, index) => {
                state.taskList = value.noiDung;
                state.id = index + 1;
                state.status = true;
            })

        default:
            break;
    }
    return { ...state };
}

export default taskReducer;